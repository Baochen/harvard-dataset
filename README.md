# Harvard dataset #

**Data:** All monitors were initialized and then mailed to participants, worn, and then returned in the mail. Hence, the raw data files contain a few days of data before and after the 7-day duration participants wore the sensors.
There are two raw data files for each participant; one for wrist and the other for hip data. The files for wrist and hip are differentiated with a letter, W/H, in the name of the file. For example 181384366HRAW.csv and 181384366WRAW.csv are the names of the files for Hip and Wrist of a participant with the ID of 181384366. This ID could be used to match zip code and the logs info of this participant.

**Location info:** Monitor data are in Eastern Standard Time (EST) but some of the log times are in a different time zone depending on the location of the participant. The difference in time for participants outside the EST zone needs to be considered. “zipcodes.csv” contains zip code information of more than 20000 participants including the 98 participants from whom we have raw acceleration data.

**Logs:** In the “wristlog.csv” file, each row has data for one subject over around 7 days of data. Logs include whether the dominant hand are left, right or ambidextrous and if the monitor hand is left or right. In addition, the times in bed and out of bed of the subjects during the 7 day wear period were logged.

Wear dates and times are entered individually in columns for month, day and year and hour and min.

* TOOB= Time Out Of Bed

* TIB= Time In Bed

* Dominant/Monitor Wrist: 1=left, 2=right, 3= ambidextrous


Participants were asked to wear wrist monitors continuously worn for the duration of the 7-day study. Hip monitors were worn during waking hours only for about 70% of the subjects. The same logs can be used to gather self-reported wear times for the hip monitors as well.

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact