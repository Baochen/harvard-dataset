package processing;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class ZipTimeManager {
	public static void makeTimeZip() {
		String path = "C:/Users/Aida/Data/Harvard I-Min data/";		
		Set<String> mySet = createIDset(path + "3.RAW/");
		String data [][] = new String [mySet.size()][4]; //ID,zip,time,ds
		findZip(mySet, data,  path + "Harvard DataSet info/zipcodes.csv");	
		findTime(data, path + "Harvard DataSet info/", "timezone.csv", "timeZip.csv");				
	}
	
	//create the ID set
	public static Set<String> createIDset(String inPath) {
		String[] allIDs = new String[196];
		Set<String> IDset = new HashSet<String>();
		File folder = new File(inPath);
		if (folder.isDirectory()) {
			allIDs = folder.list(new FilenameFilter() {
				@Override
				public boolean accept(File dir, String filename) {
					return filename.contains("RAW");
				}
			});
		}
		for (int i = 0; i < allIDs.length; i++ ) {
			IDset.add(allIDs[i].substring(0, allIDs[i].length()- 8));
		}
		return IDset;
	}
	
	//for each ID find the zip code
	public static void findZip(Set<String> set, String[][] myData, String zipFilePathName) {
		int i = 0;
		for (String s: set) {			
			BufferedReader in;			
			try {
				in = new BufferedReader(new FileReader(zipFilePathName));
				String line;
				while ((line = in.readLine()) != null) {
					if (line.contains(s)) {
						myData[i][0] = line.trim().split(",")[0];
						myData[i][1] = line.trim().split(",")[1];
						i++;
						break;
					}				
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}			
		}
	}
	
	//for each zip code find the time zone & if daylight saving is applied there
	public static void findTime(String[][] myData, String path, String inFile, String outFile) {
		int i = 0;
		PrintWriter out = null;
		BufferedReader in = null;
		try {
			out = new PrintWriter(new FileWriter(path + outFile, true));
			for (int j =0; j < myData.length; j++) {									
				in = new BufferedReader(new FileReader(path + inFile));
				String line;
				while ((line = in.readLine()) != null) {
					if (line.contains(myData[j][1])) {
						myData[i][2] = line.trim().split(",")[5].replace("\"", "");
						myData[i][3] = line.trim().split(",")[6].replace("\"", "");
						//System.out.println(myData[i][0] + "," + myData[i][1] + "," + myData[i][2] + "," + myData[i][3]);
						out.append(myData[i][0] + "," + myData[i][1] + "," + myData[i][2] + "," + myData[i][3] + "\n");
						i++;
						break;
					}				
				}						
			}
			if (in != null) {
				in.close();
			}
			if (out != null) {
				out.flush();
				out.close();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	}
	
	//if daylight saving is applied check the date of the study to see if it applied
	//correct the time in the file
	public static void correcTime(String ID, String path, String inFile, String outFile) {
		SimpleDateFormat mHealthFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		PrintWriter out = null;
		BufferedReader in = null;
		String line;
		String tzLine;
		int diff = 0;
		try {
			String myPath = "C:/Users/Aida/Data/Harvard I-Min data/Harvard DataSet info/timeZip.csv";	
			BufferedReader tzReader = new BufferedReader(new FileReader(myPath));
			while ((tzLine = tzReader.readLine()) != null) {
				if (tzLine.contains(ID)) {					
					break;
				}
			}
			int tz = Integer.parseInt(tzLine.trim().split(",")[2]);
						
			in = new BufferedReader(new FileReader(path + inFile));	
			
			if (tzLine != null) {				
				diff = -tz - 5; // the origin is Boston area ; GMT - 5
				if (tzLine.trim().split(",")[3].equals("1")) { //ds=1
					if(diff == 0) {
						if (tzReader != null) {
							tzReader.close();
						}
						System.out.println("No correction time is needed for " + inFile);
						return;
					} else {
						//apply the correction for all the file
						out = new PrintWriter(new FileWriter(path + outFile, true));
						out.append("TIME_STAMP,VALUE");
						
						line = in.readLine();//ignore label
						while ((line = in.readLine()) != null) {
							Date date = mHealthFormat.parse(line.split(",")[0]);
							date.setTime(date.getTime() + (diff * 3600000));
							out.append( mHealthFormat.format(date) + "," + line.split(",")[1] + "\n");
						}
						System.out.println("Times are corrected for " + inFile);
					}
				} else {//ds=0 //the only one ID: 300754239					
					Date start = mHealthFormat.parse("2013-03-10 02:00:00.000"); // March 10th 2013
					Date end = mHealthFormat.parse("2013-11-03 02:00:00.000"); // November 3rd 2013
					out = new PrintWriter(new FileWriter(path + outFile, true));
					out.append("TIME_STAMP,VALUE");
					
					line = in.readLine();//ignore label
					while ((line = in.readLine()) != null) {
						diff = -tz - 5;
						Date date = mHealthFormat.parse(line.split(",")[0]);
						if (date.before(end) && date.after(start)) {
							diff += 1; 
						}
						date.setTime(date.getTime() + (diff * 3600000));
						out.append( mHealthFormat.format(date) + "," + line.split(",")[1] + "\n");	
						line = in.readLine();
					}
					System.out.println("Times are corrected for " + inFile);
				}
			} 
			
			if (tzReader != null) {
				tzReader.close();
			}
			if (in != null) {
				in.close();
			}
			if (out != null) {
				out.flush();
				out.close();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	}
	
}
